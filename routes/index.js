var express = require('express');
var router = express.Router();
var pg = require('pg');
var conString = "postgres://msfdev:msf_pass@172.16.159.132/msf_dev_db";
//var viva = require('Si');

router.get('/hosts', function (req, res) {
    var results = [];

    // Get a Postgres client from the connection pool
    pg.connect(conString, function(err, client, done) {
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({ success: false, data: err});
        }

        var query = client.query("SELECT * FROM hosts;");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            console.log(results);
            res.render('hosts',{hostslist : results});
        });
    });
});

router.get('/services', function (req, res) {
    var results = [];

    // Get a Postgres client from the connection pool
    pg.connect(conString, function(err, client, done) {
        if(err) {
            done();
            console.log(err);
            return res.status(500).json({ success: false, data: err});
        }

        //var query = client.query("SELECT hosts.name,services.name,services.proto,services.port FROM services INNER JOIN hosts ON services.host_id=hosts.id;");
        var query = client.query("SELECT hosts.address,services.name,services.proto,services.port FROM services INNER JOIN hosts ON services.host_id=hosts.id;");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            console.log(results);
            res.render('services',{servicelist : results});
        });
    });
});

router.get('/test', function (req, res) {
    var graph = viva.Graph.graph();
    graph.addLink(1, 2);

    var renderer = viva.Graph.View.renderer(graph);
    renderer.run();
});

var io = require('socket.io')(31337);
var metasploitClient = require('msfnode');
var msfClient;

var appName = "Shanks";

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: appName});
});

router.post('/', function(req, res){
    return;
});


//Socket Io stuff
io.on('connection', function (client) {
    client.on('message', function (data) {
        commandEntered(data, client);
    });

});



//msfapi stuff

var onConnect = function(err,token) {
    if (err) {
        console.log(err.error_message);
        process.exit(0);
    }
    io.send('msfnode', 'Connected to metsploit rpc');

    metasploitVersion();
};

var commandEntered = function(data, client){
    console.log(data + "\n");
    if(data === 'modules'){
        msfClient.exec(['core.module_stats'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r));
            client.emit('msfnode', 'Currently loaded modules:');
            client.emit('msfnode', 'exlpoits: ' + r.exploits);
            client.emit('msfnode', 'auxiliary: ' + r.auxiliary);
            client.emit('msfnode', 'post: ' + r.post);
            client.emit('msfnode', 'encoders: ' + r.encoders);
            client.emit('msfnode', 'nops: ' + r.nops);
            client.emit('msfnode', 'payloads: ' + r.payloads);
        });
    }else if(data === 'list exploits'){
        msfClient.exec(['module.exploits'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }else if(data === 'list auxiliary'){
        msfClient.exec(['module.auxiliary'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }else if(data === 'list post'){
        msfClient.exec(['module.post'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }else if(data === 'list encoders'){
        msfClient.exec(['module.encoders'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }else if(data === 'list nops'){
        msfClient.exec(['module.nops'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }else if(data === 'list payloads'){
        msfClient.exec(['module.payloads'],function(err,r) {
            if (err) return console.log('Error: '+err);
            console.log((r.modules));
            r.modules.forEach(function(mod){
                client.emit('msfnode', mod);
            })
        });
    }
};

msfClient  = new metasploitClient({
    login:'myLogin',
    password:'myPassword',
    host:'localhost',   // optional
    port:55553,         // optional
    protocol:'https',   // optional
    apiVersion:'1.0',   // optional
    apiPath:'/api/'     // optional
});

msfClient.on('connected',onConnect);

var metasploitVersion = function() {

    // Next line is the interesting part.
    //
    // Do not care about token, it will automaticaly
    // be added as the second arguments
    //
    // The first item of the array if the RPC call
    // you want to fire, as described in the
    // metasploit remote api documentation

    var args = ['core.version'];

    msfClient.exec(args,function(err,r) {

        if (err) return console.log('Error: '+err);

        console.log('-> Version: '+r.version);
        io.send('msfnode', '-> Version: '+r.version);
        console.log('-> Api: '+r.api);
        io.send('msfnode','-> Api: '+r.api);
        console.log('-> Ruby: '+r.ruby);
        io.send('msfnode','-> Ruby: '+r.ruby);
    });
};

module.exports = router;
